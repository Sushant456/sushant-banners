'use strict';
~ function() {
    var 
    	bgExit = document.getElementById("bgExit"),
        count=0,
        bgDrop = document.getElementById("bgDrop"),
        biebersDrop = document.getElementById("biebersDrop"),
        ad = document.getElementById("mainContent");

    window.init = function() {
		bgExit.addEventListener("click", clickHandler,false)
		
		createDrops(bgDrop,-50,"drop");
		createDrops(biebersDrop,150,"drop1");


        var tl = new TimelineMax({});
        tl.set(ad, { perspective: 1000, force3D: true })
        
		.to(["#bieber,#bieberBg"],7,{ scale:1.45,x:20,y:-37, ease: Sine.easeInOut },"+=1.5")
		.to(["#bieberBlur,#bieberBgBlur"],5,{opacity:1, scale:1,x:-3,y:-13, ease: Sine.easeInOut },"-=5.5")
		

		function createDrops(dropPos, dropLeftPos,dropName){
			for(var i=0; i<5; i++){
			var	drops=document.createElement("div");
				drops.id = "drop"+ i;
				drops.className=dropName;
				dropPos.appendChild(drops);
				drops.style.opacity=0;
				drops.style.top = (Math.random()*200) + "px";
				drops.style.left = (145 + Math.random()*dropLeftPos) + "px";
		

			var dropOpacity=Math.random();
			var speed=Math.floor(Math.random()*5);
			var apper=Math.floor(Math.random()*5);
	
			TweenMax.to( drops,1, {opacity:dropOpacity, ease: Sine.easeInOut});
			TweenMax.to( drops, speed, {delay:apper,y:250, ease: Sine.easeInOut});
					
			}

		}
    

    function clickHandler(e){
        e.preventDefault();
        window.open(window.clickTag);
    }
}
}();
